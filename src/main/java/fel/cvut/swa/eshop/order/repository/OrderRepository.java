package fel.cvut.swa.eshop.order.repository;

import fel.cvut.swa.eshop.order.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}

