package fel.cvut.swa.eshop.order.model;

public enum EventType {
    CREATED,
    DELETED,
    UPDATED
}
