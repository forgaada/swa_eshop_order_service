package fel.cvut.swa.eshop.order.controller;

import fel.cvut.swa.eshop.order.model.Contents;
import fel.cvut.swa.eshop.order.model.Order;
import fel.cvut.swa.eshop.order.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/order")
@AllArgsConstructor
public class OrderController {

        private final OrderService orderService;

        @GetMapping(path = "/all")
        public HttpEntity<List<Order>> getAll() {
                return new ResponseEntity<>(orderService.findAllOrders(), HttpStatus.OK);
        }

        @GetMapping(path = "/{id}")
        public HttpEntity<Order> get(@PathVariable Integer id) {
                Optional<Order> order = orderService.findById(id);
                if (order.isEmpty()) {
                        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
                } else {
                        return new ResponseEntity<>(order.get(), HttpStatus.OK);
                }
        }

        @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        public ResponseEntity<Order> create(@RequestBody Contents contents, @RequestHeader String username) {
                return new ResponseEntity<>(orderService.create(contents, username), HttpStatus.CREATED);
        }

        @DeleteMapping(path = "/{id}")
        void delete(@PathVariable Integer id) {
                Optional<Order> order = orderService.findById(id);
                if (order.isEmpty()) {
                        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
                } else {
                        orderService.delete(order.get());
                }
        }

}
