package fel.cvut.swa.eshop.order.service;

import fel.cvut.swa.eshop.order.model.Contents;
import fel.cvut.swa.eshop.order.model.Event;
import fel.cvut.swa.eshop.order.model.Order;
import fel.cvut.swa.eshop.order.repository.OrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
@Slf4j
public class OrderService implements ApplicationListener<ApplicationReadyEvent> {

    private static final String TOPIC = "orders";
    private final OrderRepository orderRepository;

    @Autowired
    private KafkaTemplate<String, Event> kafkaTemplate;

    public List<Order> findAllOrders() {
        return orderRepository.findAll();
    }

    public Optional<Order> findById(Integer id) {
        return orderRepository.findById(id);
    }

    public Order create(Contents contents, String username) {
        log.info(String.format("creating new order for user %s", username));
        Order order = new Order();
        order.setCreatedAt(new Date(Calendar.getInstance().getTimeInMillis()));
        order.setOwner(username);
        order.setDescription(contents.getDescription());
//        sendMessage(new Event(contents.items, EventType.CREATED));
        return orderRepository.save(order);
    }

    public void delete(Order order) {
        log.info(String.format("delete order with id %s", order.getId()));
        orderRepository.delete(order);
    }

    public void sendMessage(Event event) {
        log.info(String.format("#### -> Producing message -> %s", event.toString()));
        this.kafkaTemplate.send(TOPIC, event);
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        this.create(new Contents(new HashMap<>(){{put("item_01", 1);}}, "test order 1"), "test_user");
        this.create(new Contents(new HashMap<>(){{put("item_02", 2);}}, "test order 2"), "test_user");
        this.create(new Contents(new HashMap<>(){{put("item_03", 5);}}, "test order 3"), "test_user");
    }
}
