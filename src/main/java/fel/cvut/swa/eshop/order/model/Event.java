package fel.cvut.swa.eshop.order.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@AllArgsConstructor
@Getter
@Setter
public class Event {
    private Map<String, Integer> items;
    private EventType type;
}
