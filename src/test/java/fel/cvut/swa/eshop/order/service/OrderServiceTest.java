package fel.cvut.swa.eshop.order.service;

import fel.cvut.swa.eshop.order.model.Contents;
import fel.cvut.swa.eshop.order.model.Event;
import fel.cvut.swa.eshop.order.model.Order;
import fel.cvut.swa.eshop.order.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private KafkaTemplate<String, Event> kafkaTemplate;

    private OrderService orderService;

    @BeforeEach
    public void setUp() {
        orderService = new OrderService(orderRepository, kafkaTemplate);
    }

    @Test
    public void testCreate() {
        // arrange
        Contents contents = new Contents(new HashMap<>(){{put("item_01", 1);}}, "Single item");
        String username = "test_user";
        Order o = new Order();
        o.setDescription(contents.getDescription());
        o.setOwner(username);

        when(orderRepository.save(any())).thenReturn(o);

        // act
        Order result = orderService.create(contents, username);

        // assert
        assertNotNull(result);
        assertEquals(result.getDescription(), contents.getDescription());
        assertEquals(result.getOwner(), username);

        ArgumentCaptor<Order> orderCaptor = ArgumentCaptor.forClass(Order.class);

        verify(orderRepository, times(1)).save(orderCaptor.capture());
        assertEquals(username, orderCaptor.getValue().getOwner());
        assertEquals(contents.getDescription(), orderCaptor.getValue().getDescription());
    }
}